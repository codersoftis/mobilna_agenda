$(document).ready(function(){

	$('#user_menu').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#lists_menu').find('ul').find('li').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#new_list_button').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#user_menu').on('click',function(){
		$('#menu').slideToggle('fast');
	});

	$('#new_list_form').find('input[type=submit]').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#menu').find('ul').find('li').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#close_log').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#close_edit_list').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#close_edit_task').hover(function(){
		$(this).toggleClass('pointer');
	});

	$('#new_list_button').on('click',function(){
		var span = $(this).find('span');
		if($('#cover_new').height() == 125){
			$('#cover_new').animate({height:"50px"},'fast');
			span.text('+');
			$('#new_list_form').find('input[type=text]').val('');
			$('#new_list_form').hide();
		}
		else{
			$('#cover_new').animate({height:"125px"},'fast');
			span.text('-');
			$('#new_list_form').show();
		}
	});

	function return_no(){
		$('#lists_menu').find('ul').find('li').each(function(){
			var href = $(location).attr("href");
			var segment = href.substr(href.lastIndexOf('/') + 1);
			var id = $(this).data('list_id');
			if (id == segment){
				$.post( "main/remove_from_pattern", {list_no: $(this).data('list_no')} );
				return $(this).data('list_no');
			}
		});
	}

	$('#edit_window').find('#edit_form').submit(function(){
		alert(return_no());
		console.log('return_no()');
		return_no();
		return;
	});



	$('#lists_menu').find('ul').find('li').each(function(){
		var href = $(location).attr("href");
		var segment = href.substr(href.lastIndexOf('/') + 1);
		var id = $(this).data('list_id');
		if (id == segment){
			var img = $(this).find('img');
			$(this).css('background','#aadcff');
			img.show();
			img.click(function(){
				$('#edit_window').show();
			});
			return false;
		}
	});

	$('#show_log').click(function(){
		$('#tasks').hide();
		$('#log').show();
	});

	$('#log').find('#close_log').click(function(){
		$('#tasks').show();
		$('#log').hide();
	});

	$('#edit_window').on('click', '#close_edit_list', function(){
	$('#edit_window').hide()
	} );

	$('#tasks.edit').on('click', '#close_edit_task', function(){
		$('#tasks.edit').hide()
		$('#tasks').show();
    $link = document.URL.split("/");
    window.location.href=$link.splice(0,$link.length-2).join("/");
	} );

	$('#log').find('table').find('tr:even').addClass('gray');
});