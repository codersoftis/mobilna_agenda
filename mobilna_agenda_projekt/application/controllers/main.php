<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Main extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->session->set_userdata(array('redirect_url'=> $url));
			redirect('login');
		}
		$this->load->model('lists_model');
		$this->load->model('tasks_model');
		$this->load->model('log_model');
		$this->load->library('form_validation');
	}

	function index($list=NULL){
		$id = $this->uri->segment(2);
		if($list !== NULL){
			if(!$this->lists_model->if_list($id)){
				redirect('main/absent_view');
			}
			if(!$this->lists_model->check_user($id)){
				redirect('main/forbidden_view');
			}
		}
		if($this->lists_model->check_user($id)){
			$zoznamy = $this->sort();
			foreach($zoznamy as $i){
				$data['zoz'.$i['id']] = $this->lists_model->return_tasks($i['id']);
				$data['complete'.$i['id']] = $this->tasks_model->num_of_completed($i['id']);
			}
			$data['pocet'] = $this->lists_model->num_of_list();
			$data['zoz'] = $zoznamy;
			$data['last_id'] = $this->lists_model->last_list_id();
			$data['edit'] = $this->uri->segment(3);
			$data['ulohy'] = $this->tasks_model->vrat_ulohy_podla_parent($id);
      $data['ulohyNieVZozname'] = $this->tasks_model->getAllTasksNotInArray($id);
			$data['num_complete'] = $this->tasks_model->num_of_completed($id);
			$data['result_log'] = $this->log_model->return_log();
			$this->load->view('zoznam_view',$data);
		}
	}

	function absent_view(){
		$this->load->view('absent_view');
	}

	function forbidden_view(){
		$this->load->view('zakaz_view');
	}

	function sort(){
		$idd = $this->lists_model->return_pattern();
		$id = explode(" ", $idd[0]['list_seq']);
		$lists =  $this->lists_model->return_lists();
	    $ordered = array();


	    foreach($id as $key) {
	        if(array_key_exists($key,$lists)) {
	            $ordered[$key] = $lists[$key];
	            unset($lists[$key]);
	        }
	    }
	    return $ordered;
	}
}
?>