<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Tasks extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->session->set_userdata(array('redirect_url'=> $url));
			redirect('login');
		}
		$this->load->model('tasks_model');
		$this->load->model('lists_model');
		$this->load->library('form_validation');
	}

	function vytvor_ulohu($id){
		$this->form_validation->set_rules('taskName', 'Názov úlohy', 'trim|required|strip_tags');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$this->tasks_model->uloz_ulohu($this->uri->segment(3));
			redirect('zoznam/'.$id);
		}
		else{
			$this->session->set_flashdata('zle','<p class="error wrong_message">Chyba</p>');
			redirect('zoznam/'.$id);
		}
	}

	function edit_uloha($id){
		if(isset($_POST['delete'])){
			$this->tasks_model->delete_ulohu($id);
			redirect('zoznam/'.$_POST['taskParentId']);
		}
		else if(isset($_POST['uloz'])){
			$this->tasks_model->edit_ulohu($id);
			redirect('zoznam/'.$_POST['taskParentId']);
		}
	}

	function changeTaskCompleted(){
		$data['id'] = $_POST['id'];
		$data['completed'] = $_POST['completed'];
		if($data['completed']=="true"){
			$this->tasks_model->change_to_incomplete($data['id']);
		}
		else if($data['completed']=="false"){
			$this->tasks_model->change_to_complete($data['id']);
		}
		echo "completed";
	    // UPDATE TABULKY, CI JE TASK HOTOVY, ALEBO NIE $_POST['id'], $_POST['completed']
	}
	
	function changeOrderUp(){
	    $data['id'] = $_POST['id'];
	    $this->tasks_model->changeOrderUp($data['id']);
	}
	
	
	/////// SCRIPT SPUSTENY NA CRONE ////////
	function clearCompletedTasks(){
	    //echo "CLEARUJEEESSAAAAALLALALAL1";
	    if($this->uri->segment(3)=="jgh561we8rqw4e8fa6a5e7r9q"){
	        
	        $this->tasks_model->clearCompleted();
	        //echo "CLEARUJEEESSAAAAALLALALAL2";
	    }
	}

}
?>