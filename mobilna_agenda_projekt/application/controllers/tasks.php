<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Tasks extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->session->set_userdata(array('redirect_url'=> $url));
			redirect('login');
		}
		$this->load->model('tasks_model');
		$this->load->model('lists_model');
		$this->load->model('log_model');
		$this->load->library('form_validation');
	}

	function vytvor_ulohu($id){
		$this->form_validation->set_rules('taskName', 'Názov úlohy', 'trim|required|strip_tags');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$this->tasks_model->uloz_ulohu($this->uri->segment(3));
      $this->log_model->save_task_to_log($this->db->insert_id(),0);
			redirect('zoznam/'.$id);
		}
		else{
			$this->session->set_flashdata('zle','<p class="error wrong_message">Chyba</p>');
			redirect('zoznam/'.$id);
		}
	}

	function edit_uloha($id,$idd){       // ked sa stlaci "edit"
		if(isset($_POST['delete'])){
			$this->tasks_model->delete_ulohu($id);
			redirect('zoznam/'.$idd);
		}
    else if (isset($_POST['deleteInList'])){
      $this->tasks_model->delete_inlist($id, $idd);
      redirect('zoznam/'.$idd);
    }
		else if(isset($_POST['uloz'])){
			$this->tasks_model->edit_ulohu($id);
			redirect('zoznam/'.$idd);
     // $this->load->view('ulohy_edit_view',$data);       // <= toto som sem dal
		}
	}

	function changeTaskCompleted(){
		$data['id'] = $_POST['id'];
		$data['completed'] = $_POST['completed'];
		if($data['completed']=="true"){
			$this->tasks_model->change_to_incomplete($data['id']);
			$this->log_model->save_task_to_log($data['id'],0);
			}
		else if($data['completed']=="false"){
			$this->tasks_model->change_to_complete($data['id']);
			$this->log_model->save_task_to_log($data['id'],4); 
		}
		echo "completed";
	    // UPDATE TABULKY, CI JE TASK HOTOVY, ALEBO NIE $_POST['id'], $_POST['completed']
	}
	
	function changeOrderUp(){
	    $data['id'] = $_POST['id'];
	    $this->tasks_model->changeOrderUp($data['id']);
	}
	
	
	/////// SCRIPT SPUSTENY NA CRONE ////////
	function clearCompletedTasks(){
	    //echo "CLEARUJEEESSAAAAALLALALAL1";
	    if($this->uri->segment(3)=="jgh561we8rqw4e8fa6a5e7r9q"){
	        
	        $this->ulohy_model->clearCompleted($this->uri->segment(2));
	        //echo "CLEARUJEEESSAAAAALLALALAL2";
	    }
	}
  function casLog(){
	    echo "CAS LOG";
	    $data['id'] = $_POST['id'];
	    $data['time'] = $_POST['time'];
	    $task = $this->ulohy_model->vrat_ulohy($data['id']);
	    $data['name'] = $task['name'];
	    $this->log_model->logTime($data);
	    print_r($data);
	    console.log($data);
	}

	function num_of_completed($id){
		$this->tasks_model->num_of_completed($id);
	}
	function zmazSplnene($id){
		
		//echo 'ide to';
		$this->tasks_model->clearCompleted($id);
		redirect('zoznam/'.$id);
	}  
  
  function connectUloha($id, $idZoznam){       // ked sa stlaci "pridaj s vazbou"
			$this->tasks_model->connect_ulohu($id, $idZoznam);
      
			redirect('zoznam/'.$idZoznam);
     // $this->load->view('ulohy_edit_view',$data);       // <= toto som sem dal
		
	}
  //function getAllTasksNotInArray($idZoznam){
  //  $this-task_model->getAllTasksNotInArray($idZoznam);
  //}
}
?>