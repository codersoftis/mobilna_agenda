<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Lists extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->session->set_userdata(array('redirect_url'=> $url));
			redirect('login');
		}
		$this->load->model('lists_model');
		$this->load->model('tasks_model');
		$this->load->library('form_validation');
	}

	function create_list($id=NULL){
		$this->form_validation->set_rules('listName', 'Názov zoznamu', 'trim|required|strip_tags');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
		if($this->form_validation->run()) {
			$this->lists_model->save_list();
			$this->lists_model->add_to_patern();
			if($id == NULL){
				redirect(base_url());
			}
			else{
				$idd = $this->lists_model->last_list_id()[0]['id'];
				redirect('zoznam/'.$idd);
			}
		}
		else{
			$this->session->set_flashdata('wrong_new_list','<p class="error wrong_message">Chyba! Prázdna položka názov.</p>');
			if($id == NULL){
				redirect(base_url());
			}
			else{
				redirect('zoznam/'.$id);
			}
		}
	}

	function delete_list($id){
		if(isset($_POST['delete'])){
			$new_index = $this->lists_model->return_new_list_index($id);
			$this->lists_model->remove_from_pattern($id);
			$this->lists_model->delete_list($id);
			if($new_index == -1){
				redirect('main');
			}
			else{
				redirect('zoznam/'.$new_index);
			}
		}
	}

	function edit_zoznam_uloh($id){
		if(isset($_POST['edit'])){
			//echo memory_get_usage().'<br>';
			$this->lists_model->edit_zoznam_uloh($id);
			redirect('zoznam/'.$id);
		}
	}

	function add_pattern(){
		$this->lists_model->add_to_patern();
	}

	function remove_from_pattern($id){
		$this->lists_model->remove_from_pattern($id);
	}

	function save_pattern(){
		$this->lists_model->save_pattern($_POST['list_no']);
	}
}
?>