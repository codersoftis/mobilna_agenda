<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Auth extends CI_Controller {
 
        function __construct()
        {
                parent::__construct();
                $this->load->model('auth_model');
        }
       
       
        function login()
        {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('user_name', 'Meno', 'trim|required');
                $this->form_validation->set_rules('user_password', 'Heslo', 'trim|required');
                // ak prejde validaciou
                if ($this->form_validation->run())
                {                      
                        if ($this->auth_model->check())
                        {
                                // vytvorime session
                                $data['uid'] = $this->auth_model->getUserData($_POST['user_name']);
                                $data['logged_in'] = true;
                                $data['meno'] = $_POST['user_name'];
                                $this->session->set_userdata($data);
                                //print_r($data);
                                $url = $this->session->userdata('redirect_url');
                                redirect('main');
                                redirect($url);
                        }
                        else
						{
							$this->session->set_flashdata('bad_login','<p class="error wrong_message error_window">Pozor! Zlé meno alebo heslo.</p>');
							echo $this->session->flashdata('bad_login');
						}
					}
				else
					{
						// chyba
					}		             
               
                $this->load->view('login_view');       
        }
       
       
        function logout()
        {
                $this->session->unset_userdata(array('meno'=>'', 'logged_in'=>'', 'uid'=>''));
                $this->session->unset_userdata($url);
                $this->login();
        }      
}