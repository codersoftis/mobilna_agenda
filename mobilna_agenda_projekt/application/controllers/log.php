<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Log extends CI_Controller {
 
        function __construct()
        {
                parent::__construct();
                $this->load->model('log_model');
                $this->load->dbutil();
                $this->load->helper('file');
        }

        function export_log(){

                header('Content-Type: Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="export.csv"');

                $returned_data = $this->log_model->return_log_to_cvs();
                //print_r($returned_data);
                /*foreach ($returned_data as $ret) {
                    print_r($ret);
                    //$ret['name'] = $this->session->all_userdata()['meno'];
                }*/
                //print_r($returned_data);
                $data = $this->dbutil->csv_from_result($returned_data, ",", "\r\n");
                print_r($data);
                //print_r($data);
        }

         function record_log(){
            $id = $_POST['id_task'];
            $action = $_POST['action'];
            $this->log_model->save_task_to_log($id,$action);
            redirect('main');
        }


}