<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Tasks_model extends CI_Model {
function __construct() {
		parent::__construct();
	}
	
	
	
	function vrat_ulohy($id){
		$select = $this->db->get_where('tasks',array('id_list'=>$id))->result_array();
		return $select;
	}
	function vrat_ulohy_podla_parent($id){
	    $select = $this->db->get_where('tasks')->result_array();
	    $result = array();
	    foreach($select as $row){
	        $aParents = explode(',',$row['parents']);
	        if(in_array($id,$aParents)){
	            $result[] = $row;
	        }
	    }
	    return $result;
	}
	
	function delete_ulohu($id){
		$this->db->delete('tasks', array('id' => $id));
	}
  
  function delete_inlist($id, $idZoznam){
		$tsk =  $this->db->get_where('tasks',array('id'=>$id))->result_array();
    foreach($tsk as $uloha){
          if($uloha['id']==$id){
            $parents = $uloha['parents'];
            $aParents = explode(',',$parents);
            if (($key = array_search($idZoznam, $aParents)) !== false) {
              unset($aParents[$key]);
            }
            $poleS = "";
      	    foreach($aParents as $prvok){
      	        $poleS.= $prvok.",";
      	    }
      	    $poleS = substr($poleS, 0, -1);
            
            
            $prnts = array(
            'parents' => $poleS
            );
          }
        }
			$this->db->where('id', $id);
			$this->db->update('tasks', $prnts);
	}
	
	
	function arrToString($arr){
	    $poleS = "";
	    foreach($arr as $prvok){
	        $poleS.= $prvok.",";
	    }
	    $poleS = substr($poleS, 0, -1);
	    return $poleS;
	}
	
	function uloz_ulohu($id_list){
	    $count = $this->db->get_where('tasks',array('id'=>1))->num_rows();
		$data = array(
		   'id_list' => $id_list,
		   'name' => $_POST['taskName'],
       	   'note' => '',
		   'complete' => FALSE,
       	   'repeat' => FALSE,
       	   'parents' => $id_list
		);

		$this->db->insert('tasks', $data);
	}
	
	function edit_ulohu($id){
		if (!($_POST['taskName'] == '')){
		    $opakuj = 1;
        if(isset($_POST['taskRepeat'])){
  		    if($_POST['taskRepeat'] == ""){ 
  		        $opakuj = 0;
  		    }
        }
        else{
          $opakuj = 0;
        }
		    
		    
			$data = array(
			   'name' => $_POST['taskName'],
         	   'note' => $_POST['taskNote'],
               'repeat' => $opakuj
			);
			$this->db->where('id', $id);
			$this->db->update('tasks', $data);
		}
	}
  function connect_ulohu($id, $idZoznam){
    $prnts = '';
		    $tsk =  $this->db->get_where('tasks',array('id'=>$id))->result_array();
        foreach($tsk as $uloha){
          if($uloha['id']==$id){
            $parents = $uloha['parents'];
            $parents = $parents.','. $idZoznam;
            $prnts = array(
            'parents' => $parents
            );
          }
        }
			$this->db->where('id', $id);
			$this->db->update('tasks', $prnts);
		
	}
  
  function getAllTasksNotInArray($idZoznam){
	    $select = $this->db->get_where('tasks')->result_array();
	    $result = array();
	    foreach($select as $row){
	        $aParents = explode(',',$row['parents']);
	        if( ! (in_array($idZoznam,$aParents))){
	            $result[] = $row;
	        }
	    }
	    return $result;
	
  }
	
	function change_to_incomplete($id){
	   $data = array(
	        'complete' => 0
	   );
	   $this->db->where('id', $id);
	   $this->db->update('tasks', $data);
	}
	function change_to_complete($id){
	   $data = array(
	        'complete' => 1
	   );
	   $this->db->where('id', $id);
	   $this->db->update('tasks', $data);
	}
	
	function changeOrderUp($id){
	    $select = $this->db->get_where('tasks')->row_array(); // alebo result array
	    $moja_uloha = vrat_ulohy($id);
	    $higherOrdId = -1;
	    $higherId = 0;
	    foreach ($select as $uloha){
	        if($uloha['order_id']< $moha_uloha['order_id']){
	            if($uloha['order_id']>$higherOrdId){
	                $higherOrdId = $uloha['order_id'];
	                $higherId = $uloha['id'];
	            }
	        }
	    }
	    $ordIdMojej = $moja_uloha['order_id'];
	    
	}
	
	function clearCompleted($id){     
	    $tsk =  $this->db->get('tasks')->result_array();   
      foreach($tsk as $uloha){
          if($uloha['complete'] == 1){
            if($uloha['repeat'] == 0){
               $this->db->delete('tasks', array('complete' => 1, 'repeat' => 0, 'id'=>$uloha['id']));
               //print_r($uloha);
            }
          }
        }
      
      //$this->db->delete('tasks', array('complete' => 1, 'repeat' => 0, 'parents'=>$id));
      
      
	    $this->db->where('repeat', 1);
	    $this->db->where('complete', 1);
	    $this->db->update('tasks', array('complete'=>0));
	}
  

	function num_of_completed($id){
		$c = 0;
		$select = $this->db->get('tasks')->result_array();
		foreach($select as $s){
			$var = explode(',',$s['parents']);
			if(in_array(strval($id), $var) and $s['complete'] == 1){
				$c++;
			}
		}
		return $c;

	}
	function return_task_name($id){
  		$data = array('id'=>$id);
		return $this->db->get_where('tasks',$data)->result_array()[0]['name'];
	}
	
}
?>