<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Tasks_model extends CI_Model {
function __construct() {
		parent::__construct();
	}
	
	
	
	function vrat_ulohy($id){
		$select = $this->db->get_where('tasks',array('id_list'=>$id))->result_array();
		return $select;
	}
	function vrat_ulohy_podla_parent($id){
	    $select = $this->db->get_where('tasks',array('id_list'=>$id))->result_array();
	    $result = array();
	    foreach($select as $row){
	        //$aParents = explode(',',$row['parents']);
	        if(in_array($id,$row)){
	            $result[] = $row;
	        }
	    }
	    return $result;
	}
	
	function delete_ulohu($id){
		$this->db->delete('tasks', array('id' => $id));
	}
	
	
	function arrToString($arr){
	    $poleS = "";
	    foreach($arr as $prvok){
	        $poleS.= $prvok.",";
	    }
	    $poleS = substr($poleS, 0, -1);
	    return $poleS;
	}
	
	function uloz_ulohu($id_list){
	    $count = $this->db->get_where('tasks',array('id'=>1))->num_rows();
		$data = array(
		   'id_list' => $id_list,
		   'name' => $_POST['taskName'],
       	   'note' => '',
		   'complete' => False,
       	   'repeat' => False,
       	   //'parents' => $id_list
		);

		$this->db->insert('tasks', $data);
	}
	
	function edit_ulohu($id){
		if (!($_POST['taskName'] == '')){
		    $opakuj = 1;
		    if($_POST['taskRepeat'] == ""){
		        $opakuj = 0;
		    }
		    
		    $parenti = $this ->arrToString($_POST['taskParents']);
		    if($parenti==""){
		        $parenti=$_POST['taskParentId'];
		    }
			$data = array(
			   'name' => $_POST['taskName'],
         	   'note' => $_POST['taskNote'],
               'repeat' => $opakuj,
               'parents' => $parenti
			);
			$this->db->where('id', $id);
			$this->db->update('tasks', $data);
		}
	}
	
	function change_to_incomplete($id){
	   $data = array(
	        'complete' => 0
	   );
	   $this->db->where('id', $id);
	   $this->db->update('tasks', $data);
	}
	function change_to_complete($id){
	   $data = array(
	        'complete' => 1
	   );
	   $this->db->where('id', $id);
	   $this->db->update('tasks', $data);
	}
	
	function changeOrderUp($id){
	    $select = $this->db->get_where('tasks')->row_array(); // alebo result array
	    $moja_uloha = vrat_ulohy($id);
	    $higherOrdId = -1;
	    $higherId = 0;
	    foreach ($select as $uloha){
	        if($uloha['order_id']< $moha_uloha['order_id']){
	            if($uloha['order_id']>$higherOrdId){
	                $higherOrdId = $uloha['order_id'];
	                $higherId = $uloha['id'];
	            }
	        }
	    }
	    $ordIdMojej = $moja_uloha['order_id'];
	    
	}
	
	function clearCompleted(){
	    $this->db->delete('tasks', array('complete' => 1, 'repeat' => 0));
	    $this->db->where('repeat', 1);
	    $this->db->where('complete', 1);
	    $this->db->update('tasks', array('complete'=>0));
	}
	
}
?>