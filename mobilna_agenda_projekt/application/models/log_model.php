<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Log_model extends CI_Model
{
   function return_log(){
    $id = $this->session->all_userdata()['uid'];
    return $this->db->get_where('log',array('id_user'=>$id))->result_array();
   }

   function return_log_to_cvs(){
    $id = $this->session->all_userdata()['uid'];
    $select = $this->db->get_where('log',array('id_user'=>$id));
    return $select;
   }

  // pridane

  function set_record_of_task($id_task,$set){
    $data = array('casLog'=>$set);
    $this->db->where('id',$id_task);
    $this->db->update('tasks',$data);
  }

  function return_action($action){
    switch($action){
      case 0:
        $action = 'Vytvorená';
        break;
      case 1:
        $action = 'Začatá';
        break;
      case 2:
        $action = 'Zastavená';
        break;
      case 3:
        $action = 'Zmazaná';
        break;
      case 4:
        $action = 'Splnená';
    }
    return $action;
  }

  function save_task_to_log($id,$action1){
    $action = $this->return_action($action1);
    $id_user = $this->session->all_userdata()['uid'];
    if($id==NULL){
      $id = $this->db->insert_id();
    }
    $this->load->model('tasks_model');
    $task = $this->tasks_model->return_task_name($id);
    $time = date('Y-m-d H:i:s');
    $data = array('task'=>$task,'action'=>$action,'time'=>$time,'id_user'=>$id_user);
    $this->db->insert('log',$data);

    if($action1 == 1){
      $this->set_record_of_task($id,1);
      console.log($action1);
    }
    else{
      $this->set_record_of_task($id,0);
    }
  }
}