<?php if(!defined("BASEPATH")) exit("No direct script acces allowed");

class Lists_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	function return_lists(){
		$id = $this->session->all_userdata()['uid'];
		$select = $this->db->get_where('lists',array('idu'=>$id))->result_array();
		return $select;
	}

	function num_of_list(){
		$id = $this->session->all_userdata()['uid'];
		return $this->db->get_where('lists',array('idu'=>$id))->num_rows();
	}

	function return_tasks($id){
		$c = 0;
		$select = $this->db->get('tasks')->result_array();
		foreach($select as $s){
			$var = explode(',',$s['parents']);
			if(in_array(strval($id), $var)){
				$c++;
			}
		}
		return $c;
	}

	function check_user($zoz){
		$id = $this->session->all_userdata()['uid'];
		$this->db->select('id');
		$select1 = $this->db->get_where('user_data',array('id'=>$id))->result_array();
		$this->db->select('idu');
		$select2 = $this->db->get_where('lists',array('id'=>$zoz))->result_array();
		if($zoz == NULL){
			return TRUE;
		}
		else{
			return $select1[0]['id'] == $select2[0]['idu'];
		}
	}

	function save_list(){
		$id = $this->session->all_userdata()['uid'];
		$data = array(
		   'idu' => $id,
		   'name' => $_POST['listName'],
		   'complete' => False
		);

		$this->db->insert('lists', $data);
	}

	function last_list_id(){
		$this->db->select_max('id');
		$select = $this->db->get('lists')->result_array();
		return $select;
	}

	function edit_zoznam_uloh($id){
		if (!($_POST['listName'] == '')){
			$data = array(
			   'name' => $_POST['listName'],
			);
			$this->db->where('id', $id);
			$this->db->update('lists', $data);
		}
	}

	function delete_list($id){
		$this->db->delete('lists', array('id' => $id));
	}

	function return_list_index(){
		$id = $this->session->all_userdata()['uid'];
		$this->db->select('id');
		$select = $this->db->get_where('lists',array('idu'=>$id))->result_array();
		return $select;
	}

	function if_list($id){
		if($id==NULL){
			return TRUE;
		}
		$select = $this->db->get_where('lists',array('id'=>$id))->result_array();
		if($select){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function return_pattern(){
		$id = $this->session->all_userdata()['uid'];
		$this->db->select('list_seq');
		$select = $this->db->get_where('user_data',array('id'=>$id))->result_array();
		return $select;
	}

	function add_to_patern(){
		$current = $this->return_pattern();
		if($current[0]['list_seq'] != NULL){
			$current = explode(" ", $current[0]['list_seq']);
			$new_index = max($current)+1;
			array_push($current, $new_index);
		}
		else{
			$current = [0];
		}
		$current = implode(" ",$current);
		$this->save_pattern($current);
	}


	function remove_from_pattern($no){
		$lists = $this->return_list_index();

		foreach($lists as $l=>$val){
			if($lists[$l]['id'] == $no){
				$index = $l;
			}
		}

		$current = $this->return_pattern();
		$current = explode(" ", $current[0]['list_seq']);

		if($index != max($current)){
			foreach ($current as $i) {
				if($current[$i] > $index){
					$current[$i] = $current[$i]-1;
				}
			}
		}

		$key = array_search($index, $current);
		unset($current[$key]);

		$current = implode(" ",$current);
		$this->save_pattern($current);
	}

	function return_new_list_index($no){
		$lists = $this->return_list_index();
		$pattern = $this->return_pattern();
		$pattern = explode(' ', $pattern[0]['list_seq']);
		$index = -1;

		foreach($lists as $l=>$val){
			if($lists[$l]['id'] == $no){
				$index = $l;
			}
		}

		$pat_index = array_search($index, $pattern);

		if(sizeof($pattern) == 1){
			$new_index = -1;
			return $new_index;
		}
		else{
			if($pat_index == sizeof($pattern)-1){
				$i = $pattern[$pat_index-1];
				$new_index = $lists[$i]['id'];
				return $new_index;
			}
			else{
				$i = $pattern[$pat_index-1];
				$new_index = $lists[$i]['id'];
				return $new_index;
			}
		}
	}

	function return_key_of_list($no){
		$key = array_search($index, $current);
		return $key;
	}

	function save_pattern($seq){
		$id = $this->session->all_userdata()['uid'];
		$this->db->where('id', $id);
		$data = array('list_seq'=>$seq);
		$this->db->update('user_data', $data);
	}


}
?>