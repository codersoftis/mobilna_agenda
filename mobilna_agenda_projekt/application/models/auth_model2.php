<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model 
{
	function check()
	{
		$select = $this->db->where('user_name', $_POST['user_name'])
						   ->where('user_password', sha1($_POST['user_password']))
						   ->get('user_data');
						   
		$select->num_rows();
		if($select > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	function getUserData($meno)
	{
		$select = $this->db->select('id')
						   ->where('user_name', $meno)
						   ->limit(1)
						   ->get('user_data');
						   
		if ($select->num_rows() > 0) return $select->result_array()[0]['id'];
		else return false;
	}

}