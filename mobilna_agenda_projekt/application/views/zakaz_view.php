<?php $this->load->view('header'); ?>
	<div class="window">
		<h1>Upozornenie!</h1>
		<p>K prezeraniu obsahu tohto zoznamu nemáte povolenie. Prihláste sa pod správnym menom!</p>
		<a href="<?= base_url() ?>">Späť na stránku</a>
	</div>
<?php $this->load->view('footer'); ?>