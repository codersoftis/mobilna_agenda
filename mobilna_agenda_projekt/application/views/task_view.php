<?php
echo form_open('tasks/vytvor_ulohu/'.$this->uri->segment(2));
$zoznam_data = array(
      'name' => 'taskName',
      'value' => set_value('taskName'),
      'placeholder' => 'Názov novej úlohy',
      );
echo form_input($zoznam_data);
$data = array('name'=>'uloz',
  'value'=>'Pridať novú úlohu',
  'class'=>'input_submit');
echo form_submit($data);
echo form_close();     


echo '<div class = "chBoxContainer">';

echo '<p>Pridaj vazbu na ulohu:</p>';
foreach($ulohyNieVZozname as $task){
		          
        		      if (strlen($task['name']) > 13){
                    $name = substr($task['name'],0,13).'...';
                  }
                  else{
                    $name = $task['name'];
                  }
        			    echo '<div class="chboxAndLabel"> ';
                  echo form_open('tasks/connectUloha/'.$task['id'].'/'.$this->uri->segment(2));
                  $data = array('name'=>'zmaz',
                    'value'=>$name,
                    'class'=>'input_submit',
                    'title'=>$task['name']);
                  //$option= array(
                    //    'uloha'=>$name);
                  //echo form_dropdown('jano',$option,'ve');
                 // echo form_dropdown('jano',$data,'ve');
                  echo form_submit($data);
                  echo form_close();
                  echo '</div>';
		    }    
        
        //echo '<input type = "hidden" name="taskParentId" value="'.$task['id_list'].'"/>';
        			
echo '</div>';



$dokoncene = array();
$nedokoncene = array();
foreach($ulohy as $task){
   if($task['complete']==0){
       $nedokoncene[]=$task;
   }
   else{
       $dokoncene[]=$task;
   }
}
?>
<ul>
<?php
//print_r($nedokoncene);
foreach ($nedokoncene as $task) {
  if (strlen($task['name']) > 34){
    $name = substr($task['name'],0,34).'...';
  }
  else{
    $name = $task['name'];
  }
  if($task['casLog'] == 0){
    echo '<li title="'.$task['name'].'"><input type = "checkbox" value="0" class="completedchbox" data-task-id="'.$task['id'].'"></input>'.$name.'<div class="toggleTimer" data-task-id="'.$task['id'].'"><img src="'.'http://da.matej.sk/mobilna_agenda/images/play.png'.'" id="buton1" width="20" height="20"" title="Spusti nahravanie"></div> <a href="'.base_url('zoznam/'.$this->uri->segment(2).'/'.$task['id'].'/edit').'" class="settings_button"><img src="'.'http://da.matej.sk/mobilna_agenda/images/coico.png'.'" title="Uprav"></a> </li>';
  }
  else{
    echo '<li title="'.$task['name'].'"><input type = "checkbox" value="0" class="completedchbox" data-task-id="'.$task['id'].'"></input>'.$name.'<div class="toggleTimer" data-task-id="'.$task['id'].'"><img src="'.'http://da.matej.sk/mobilna_agenda/images/stop.png'.'" id="buton1" width="20" height="20"" title="Zastav nahravanie"></div> <a href="'.base_url('zoznam/'.$this->uri->segment(2).'/'.$task['id'].'/edit').'" class="settings_button"><img src="'.'http://da.matej.sk/mobilna_agenda/images/coico.png'.'" title="Uprav"></a> </li>';
  }
}
// foreach ($dokoncene as $task) {
//   if (strlen($task['name']) > 13){
//     $name = substr($task['name'],0,13).'...';
//   }
//   else{
//     $name = $task['name'];
//   }
//   echo '<li class="complete_task" title="'.$task['name'].'"><input type = "checkbox" value="1"  class="completedchbox" data-task-id="'.$task['id'].'" checked></input>'.$name.'<a href="'.base_url('zoznam/'.$this->uri->segment(2).'/'.$task['id'].'/edit').'" class="settings_button"><img src="'.'http://da.matej.sk/mobilna_agenda/images/coico.png'.'"></a></li>';
// }

echo form_open('tasks/zmazSplnene/'.$this->uri->segment(2));
$data = array('name'=>'zmaz',
  'value'=>'Opakuj zoznam',
  'class'=>'input_submit');
echo form_submit($data);
echo form_close();

?>
</ul>

<p>Splnené úlohy: <?php echo $num_complete; ?></p>
<ul>
  <?php
  foreach ($dokoncene as $task) {
  if (strlen($task['name']) > 13){
    $name = substr($task['name'],0,13).'...';
  }
  else{
    $name = $task['name'];
  }
  echo '<li class="complete_task" title="'.$task['name'].'"><input type = "checkbox" value="1"  class="completedchbox" data-task-id="'.$task['id'].'" checked></input>'.$name.'<a href="'.base_url('zoznam/'.$this->uri->segment(2).'/'.$task['id'].'/edit').'" class="settings_button"><img src="'.'http://da.matej.sk/mobilna_agenda/images/coico.png'.'"></a></li>';
}
?>
</ul>

<script type="text/javascript">
    $(".toggleTimer").click(function(e){
        var ideckoLog = $(e.target);
      e.preventDefault();
      $(".toggleTimer").toggleClass("run");
      $.ajax({
        url:"<?php echo base_url('tasks/casLog')?>",
        data: "id="+ideckoLog.data("taskId"),
        type: "POST"
      }); 
    });
  </script>
  <script>
    $(".completedchbox").click(function(e){
        var idecko = $(e.target);
      $.ajax({
        url: "<?php echo base_url('tasks/changeTaskCompleted')?>",
        data: "id="+idecko.data("taskId")+"&completed="+!idecko.context.checked,
        type: "POST"
      }).done(function(data){
        console.log(data);
        if(data=="completed"){
            location.reload();
        }
      }).fail(function( jqXHR, textStatus, errorThrown ) { console.log("fail");
      });
    });
    $(".chngBttUp").click(function(e){
      var idecko = $(e.target);
      $.ajax({
        url: "<?php echo base_url('tasks/changeOrderUp')?>",
        data: "id="+idecko.data("taskId"),
        type: "POST"
      }).done(function(data){
        console.log(data);
      }).fail(function( jqXHR, textStatus, errorThrown ){ console.log("fail");
        
      });
      
    });
  </script>
  <script>
    $('.toggleTimer').find('img').click(function(){
      var id_task = $(this).parent().data('task-id');
      var src = $(this).attr('src');
      if(src.indexOf('stop.png') != -1){

        var action = 2;
      }
      else{
        var action = 1;
      }

      $.post("<?php echo base_url('log/record_log')?>",{id_task:id_task,action:action});
      location.reload();
    });
  </script>
  <script>
  $('input[name="uloz"]').click(function(){
    if($('input[name="taskName"]').val() == ''){
      alert('Názov úlohy je povinný!');
    }
  });
  </script>