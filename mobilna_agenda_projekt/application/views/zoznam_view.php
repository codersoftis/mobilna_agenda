<?php
$this->load->view('header');
$this->load->view('main_view');
?>

<div id="edit_window" class="window input_text">
	<h1>
		Nastavenie zoznamu úloh
	</h1>
	<?php
		$segment = $this->uri->segment(2);
		foreach($zoz as $list){
		        if($list['id']==$this->uri->segment(2)){
		        	$aktualny_zoznam = $list['name'];
		        }
		     }
		$attr = array("id"=>"#edit_form");
		echo form_open('lists/edit_zoznam_uloh/'.$segment,$attr);

		$cover_new_data = array(
	              'name' => 'listName',
	              'value' => $aktualny_zoznam,
	              //'placeholder' => set_value('listName'),
	              //'placeholder' => 'Kekec',
	              'class'=>'input_text',
	              );
		echo form_input($cover_new_data);
		$data = array(
			'name'=>'edit',
			'value'=>'Hotovo',
			'class'=>'input_submit pointer',
			);
		echo form_submit($data);

		echo form_close();


		$attr = array("id"=>"#delete_form");
		echo form_open('lists/delete_list/'.$segment,$attr);
		$data = array(
			'name'=>'delete',
			'value'=>'Zmazanie',
			'class'=>'input_submit pointer',
			);
		echo form_submit($data);
		echo form_close();
	?>
	<span id="close_edit_list">X</span>
</div>

<?php $this->load->view('footer'); ?>