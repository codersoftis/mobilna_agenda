	
    <div id="tasks" class="task edit">      
		<?php
		    $taskFound = 0;
        //print_r($ulohy)
			$idd = $this->uri->segment(2);
		    foreach($ulohy as $task){
		        if($task['id']==$this->uri->segment(3)){
		          echo form_open('tasks/edit_uloha/'.$task['id'].'/'.$idd);
        			echo '<input type="text" name="taskName" value="'.$task['name'].'">';
        			echo '<div class="chBoxWrapper">';
              echo '<textarea name = "taskNote" rows="6" cols="30" class="txtArea" placeholder="Zadaj poznámku">'.$task['note'].'</textarea>';
        			
        			$repeatuj = "";
        			if($task['repeat']==1){
        			    $repeatuj = "checked";
        			}
        			$mojeparent = array();
        		    $nemojparent = array();
        		    $poleparentov = explode(',', $task['parents']);
        		    foreach($zoz as $list){
        		       if(in_array($list['id'], $poleparentov)){
        		           $mojeparent[]=$list;
        		       }
        		       else{
        		           $nemojparent[]=$list;
        		       }
        		    }
    
        			echo '<div class= "opakuj"> Nastaviť opakovanie</div>';
        			echo '<input type = "checkbox" name="taskRepeat" '.$repeatuj.' class= "chbox"/>';
        			echo '<input type = "hidden" name="taskParentId" value="'.$task['id_list'].'"/>';
        			echo form_submit('uloz','Ulož zmeny');
        			echo form_submit('delete','Vymaž úlohu');
                    echo "<div class='vymaz_v_zoz'>";
                    echo form_submit('deleteInList','Vymaž úlohu v tomto zozname');
                    echo "</div>";
                    echo "<span id='close_edit_task'>X</span>";
        			echo form_close();
        			$taskFound = 1;
		      }
		    }
		?>
	</div>