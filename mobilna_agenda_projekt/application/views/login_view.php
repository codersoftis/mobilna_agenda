<?php $this->load->view('header'); ?>
<div id="login" class="window">		
	<h1>Prihlásenie</h1>
	
	<?php 
		echo form_open('login');
		$data = array(
			'name' => 'user_name',
			'value' => set_value('user_name'),
			'class' => 'input_text',
			);
		echo form_label('Meno: ').form_input($data);
		$data = array(
			'name' => 'user_password',
			'class' => 'input_text',
			);
		echo form_label('Heslo: ').form_password($data);
		$data = array(
			'name' => 'submit',
			'value' => 'Prihlásiť',
			'class' => 'input_submit input_submit_block pointer',
			);
		echo form_submit($data);
		echo form_close();
	?>
</div>
	
<?php $this->load->view('footer'); ?>