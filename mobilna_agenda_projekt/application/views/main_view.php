		<section>
			<div id="cover_menu">
				<div id="user_menu">
					<img src="<?= 'http://da.matej.sk/mobilna_agenda/images/user.png' ?>" alt="nic">
					<h1><?php echo $this->session->all_userdata()['meno']; ?></h1>
				</div>
				<div id="menu">
					<ul>
						<li id="show_log">
							Zobraz log
						</li>
						<li id="export_log">
							<a href="<?php echo base_url('log/export_log'); ?>">Exportuj log</a>
						</li>
						<li id="logout">
							Odhlasenie
						</li>
					</ul>
					<span>^</span>
				</div>
			</div>
			<?php echo $this->session->flashdata('wrong_new_list');  ?>
			<div id="lists_menu">
				<ul>
					<?php
					foreach ($zoz as $key => $value) {
						$compl = 'complete'.$value['id'];
						$a = 'zoz'.$value['id'];
						if (strlen($value['name']) > 13){
							$name = substr($value['name'],0,13).'...';
						}
						else{
							$name = $value['name'];
						}
						$sum = $$a-$$compl;
						echo '<li data-list_no='.$key.' data-list_id='.$value['id'].' title="'.$value['name'].'"><a href="'.base_url().'zoznam/'.$value['id'].'">'.$name.'</a><div class="info_li"><span>'.$sum.'</span><img src="'.'http://da.matej.sk/mobilna_agenda/images/coico.png'.'" alt="nic" title="Uprav"></div></li>';
					}
					?>
				</ul>
			</div>
			<div id="cover_new">
				<div id="new_list_button">
					<span>+</span>
				</div>
				<div id="new_list_form">
					<?php
					$segment = $this->uri->segment(2);
					echo form_open('lists/create_list/'.$segment);
					$zoznam_data = array(
				              'name' => 'listName',
				              'value' => set_value('listName'),
				              'placeholder' => 'Názov zoznamu',
				              'class'=>'input_text',
				              );
					echo form_input($zoznam_data);
					$data = array(
						'name'=>'save',
						'value'=>'Pridanie',
						'class'=>'input_submit pointer',
						);
					echo form_submit($data);
					echo form_close();
					?>
				</div>
			</div>
		</section>
		<div id="tasks">
			<?php
			if($this->uri->segment(2)){
				$data['ulohy'] = $ulohy;
				$this->load->view('task_view',$data);
        $this->load->view('ulohy_edit_view',$data);
			}
			?>
		</div>

		<div id="log">
			<?php 
			$data['log_result'] = $result_log;
			$this->load->view('log_view',$data);
			?>
		</div>

		<script>
			$('section').find('#cover_menu').find('#menu').find('ul').find('#logout').click(function(){
				$.post('<?php echo base_url("auth/logout"); ?>',function(){
					location.reload();
				});
			});
		</script>

		<script>
			$(function() {
				var ul = $("#lists_menu" ).find('ul');
				var array = [];
			    ul.sortable({update:function(event,ui){
			    	$(this).find('li').each(function(){
			    		if($.isNumeric($(this).data('list_no'))){
			    			array.push($(this).data('list_no'));
			    		}
			    	});
			    	array = array.join(' ');
			    	console.log(array);
			    	$.post('<?php echo base_url("lists/save_pattern"); ?>',{list_no:array});
			    	array = [];
				    }
				});
			    ul.disableSelection();
			});
		</script>
		<script>
			//$('#export_log').click(function(){
				//$.post('<?php echo "http://da.matej.sk/mobilna_agenda/index.php/log/export_log"; ?>',function(data){
					//alert(data);
				//});
			//});
		</script>