<?php
if($this->uri->segment(2)){

echo form_open('tasks/vytvor_ulohu/'.$this->uri->segment(2));
$zoznam_data = array(
      'name' => 'taskName',
      'value' => set_value('taskName'),
      'placeholder' => 'Názov úlohy',
      );
echo form_input($zoznam_data);
$data = array('name'=>'uloz',
  'value'=>'Pridať úlohu',
  'class'=>'input_submit');
echo form_submit($data);
echo form_close();


$dokoncene = array();
$nedokoncene = array();
foreach($ulohy as $task){
   if($task['complete']==0){
       $nedokoncene[]=$task;
   }
   else{
       $dokoncene[]=$task;
   }
}
?>
<ul>
<?php
foreach ($nedokoncene as $task) {
  echo '<li><input type = "checkbox" value="0" class="completedchbox" data-task-id="'.$task['id'].'"></input>'.$task['name'].'<a href="#" class="toggleTimer" data-task-id="'.$task['id'].'">Play</a> <a href="'.base_url('zoznam/'.$this->uri->segment(2).'/'.$task['id'].'/edit').'" class="settings_button">NICSetting</a> <a href="#" class="chngBttUp" data-task-id="'.$task['id'].'">NicOrderUp</a> <a href="#" class="chngButtDown" data-task-id="'.$task['id'].'">NicOrderDown</a></li>';
}
foreach ($dokoncene as $task) {
  echo '<li><input type = "checkbox" value="1"  class="completedchbox" data-task-id="'.$task['id'].'" checked></input>'.$task['name'].'<a href="'.base_url('zoznam/'.$this->uri->segment(2).'/'.$task['id'].'/edit').'" class="settings_button">NICEdit</a></li>';
}

?>
</ul>
<p>Splnené úlohy (0)</p> <!-- dorobit funkciu ktora vracia pocet hotovych uloh -->
<script type="text/javascript">
    $(".toggleTimer").click(function(e){
        var ideckoLog = $(e.target);
      e.preventDefault();
      $(".toggleTimer").toggleClass("run");
      $.ajax({
        url:"<?php //echo base_url('task/casLog')?>",
        data: "id="+ideckoLog.data("taskId"),
        type: "POST"
      }); 
    });
  </script>
  <script>
    $(".completedchbox").click(function(e){
        var idecko = $(e.target);
      $.ajax({
        url: "<?php //echo base_url('task/changeTaskCompleted')?>",
        data: "id="+idecko.data("taskId")+"&completed="+!idecko.context.checked,
        type: "POST"
      }).done(function(data){
        console.log(data);
        if(data=="completed"){
            location.reload();
        }
      }).fail(function( jqXHR, textStatus, errorThrown ) { console.log("fail");
      });
    });
    $(".chngBttUp").click(function(e){
      var idecko = $(e.target);
      $.ajax({
        url: "<?php //echo base_url('task/changeOrderUp')?>",
        data: "id="+idecko.data("taskId"),
        type: "POST"
      }).done(function(data){
        console.log(data);
      }).fail(function( jqXHR, textStatus, errorThrown ){ console.log("fail");
        
      });
      
    });
  </script>
<?php
}
?>